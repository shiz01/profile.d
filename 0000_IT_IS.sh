#!/bin/bash
if [ -f /etc/gentoo-release ]; then
	export IT_IS_GENTOO=1;
	if [ -d /var/db/repos/crossdev ]; then
		 export CROSSDEV_OVERLAY="/var/db/repos/crossdev"
	fi
elif [ -f /etc/debian_version ]; then 
	export IT_IS_DEBIAN=1;
elif [ -f /etc/oracle-release ]; then
	export IT_IS_ORACLE=1;
fi



