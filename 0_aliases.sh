#!/bin/bash

# for lvm2
[ -x /sbin/lvs ] && [ -x /sbin/vgs ] && [ -x /sbin/pvs ] && {
	alias lvstat='pvs -a; echo; vgs -a; echo; lvs -a; echo;'
	alias vgstat='vgs; echo; lvs;'
	alias pvstat='pvs; echo; vgs;'
}


# for ceph
[ -x /usr/bin/ceph ] && {
	alias cehp='ceph'
	alias cpeh='ceph'
	alias cephs='ceph status'

	alias grceph='ps aux | grep ceph-'

	[ -d '/etc/ceph' ] && {
        alias cdceph='cd /etc/ceph';
        alias свсузр='cd /etc/ceph';
    }

    [ -x /usr/bin/rbd ] && {
        alias rbdu='for i in $(ceph osd pool ls | grep rbd); do echo "Pool \"${i}\":"; rbd disk-usage --pool ${i}; echo ""; done'
    }
}

[ -x /usr/bin/vim ] && {
    alias vi='vim'
    alias мшь='vim'
}

[ -x /usr/bin/tmux ] && {
    alias txum='tmux'
    alias еьгч='tmux'
}

[ -x /usr/bin/tree ] && {
    alias ctree='c && tree'
}

# for ls
alias ls='ls --color=auto'
alias ll='ls -l '

wl() {
	ls -l $(which ${1});
}

# for clear
alias c='clear && clear && clear' && alias с='c'
alias cls='c && ls ' && alias сды='cls'

# for rm
alias rf='rm -rf '

[ -x /usr/bin/git ] && {
	alias gits='git status' && alias пшеы='gits'
    alias gita='git add' && alias пшеф='gita'
    alias gitc='git commit -S ' && alias пшес='gitc'
    alias gitC='git commit --no-gpg-sign --no-signoff'
	alias glp='git log -p' && alias пдз='glp'
	alias glg='git log --graph --pretty="%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset %G?" --abbrev-commit' && alias пдп='glg'


    # GIT LOG:

#	'%GF' - GPG Signature Fingerprint
#
	## Git Log Signature '%G?'
	#	* Show "G" for a good (valid) signature,
	#	* "B" for a bad signature,
	#	* "U" for a good signature with unknown validity,
	#	* "X" for a good signature that has expired,
	#	* "Y" for a good signature made by an expired key,
	#	* "R" for a good signature made by a revoked key,
	#	* "E" if the signature cannot be checked (e.g. missing key)
	#	* and "N" for no signature

}

# for error
alias exity='exit'
alias eixt='exit'
alias exit0='exit 0'
alias mkdri='mkdir'
alias mdirk='mkdir'
alias mdkir='mkdir'
alias mdkri='mkdir'

# Я упоролся.
alias кь="rm"
alias ка="rf"
alias ды="ls"
alias св='cd'

[ -x /usr/bin/wget ] && alias цпуе=wget

[ -d '/var/log' ] && alias cdl="cd /var/log" && alias свд="cdl"


[ -x /usr/bin/ncdu ] && alias ncdu='ncdu --color=dark'
[ -x /usr/bin/diff ] && alias diff='diff --color'
[ -x /usr/bin/sensors ] && alias sensors1='watch -n 1 sensors'
[ -x /usr/bin/pwndbg ] && alias pwndbg='pwndbg -ex init-pwndbg'


[ -f '/etc/gentoo-release' ] && {

	[ -d '/etc/portage' ] && {
		alias cdp="cd /etc/portage";
		alias свз="cdp";
	}

	[ -d '/var/db/repos' ] && {
		alias cdr="cd /var/db/repos";
		alias свк="cdr";
	}

	[ -x /usr/bin/ebuildtester ] && alias ebuildtester='ebuildtester \
		--threads $(($(nproc)+1)) \
		--portage-dir $(portageq get_repo_path / gentoo) \
		--ccache-dir $(portageq envvar CCACHE_DIR) \
		--shell-env "CFLAGS=$(portageq envvar CFLAGS)" \
		--shell-env "CXXFLAGS=$(portageq envvar CXXFLAGS)" \
		--shell-env "FCFLAGS=$(portageq envvar FCFLAGS)" \
		--shell-env "FFLAGS=$(portageq envvar FCFLAGS)" \
		--shell-env "RUSTFLAGS=$(portageq envvar RUSTFLAGS)" \
		--shell-env "GDCFLAGS=$(portageq envvar GDCFLAGS)" \
		--shell-env "DMDFLAGS=$(portageq envvar DMDFLAGS)" \
		--shell-env "LDCFLAGS=$(portageq envvar LDCFLAGS)" \
		--shell-env "LDFLAGS=$(portageq envvar LDFLAGS)" \
		--shell-env "ADAFLAGS=$(portageq envvar ADAFLAGS)" \
		--shell-env "PORTAGE_NICENESS=19" \
		--shell-env "EMERGE_DEFAULT_OPTS=$(portageq envvar EMERGE_DEFAULT_OPTS)" \
		--shell-env "PORTAGE_IONICE_COMMAND=$(portageq envvar PORTAGE_IONICE_COMMAND)" \
		--shell-env "CPU_FLAGS_X86=$(portageq envvar CPU_FLAGS_X86)" \
		--shell-env "LLVM_TARGETS=$(portageq envvar LLVM_TARGETS)" \
		--shell-env "VIDEO_CARDS=nouveau"'

}


