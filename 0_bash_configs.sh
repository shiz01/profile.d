#!/bin/bash

shopt -s checkwinsize

if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		source /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		source /etc/bash_completion
	fi
fi

if [ -x /usr/bin/eix ]; then
	export EIX_LIMIT=0
fi

export HISTSIZE="1000000"
export HISTTIMEFORMAT="%h %d %H:%M:%S "
export HISTIGNORE="c:с:ls:ды:ll:дд:cls:сды:history:top:ещз:htop:рещз"


