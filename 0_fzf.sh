#!/bin/bash

#
# * To add fzf support to your shell, make sure to use the right file
# * from /usr/share/fzf.
# * 
# * For bash, add the following line to ~/.bashrc:
# * 
# * 	# source /usr/share/fzf/key-bindings.bash
# * 
# * Or create a symlink:
# * 
# * 	# ln -s /usr/share/fzf/key-bindings.bash /etc/bash/bashrc.d/fzf.bash
# * 
# * Plugins for Vim and Neovim are installed to respective directories
# * and will work out of the box.
# * 
# * For fzf support in tmux see fzf-tmux(1).
#

if [[ $IS_USE_FZF ]]
then 
	[ -e /etc/bash/bashrc.d/fzf.bash ] && source /usr/share/fzf/key-bindings.bash;
fi

