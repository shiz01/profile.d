#!/bin/bash

if [[ ${IT_IS_GENTOO} ]]; then

	if [[ -f /etc/OpenCL/vendors/nvidia.icd ]]; then
        	#echo "nvidia"
        	export OCL_ICD_VENDORS=/etc/OpenCL/vendors/nvidia.icd
	else
        	if [[ -f /etc/OpenCL/vendors/pocl.icd ]]; then
                	#echo "Pocl"
                	export OCL_ICD_VENDORS=/etc/OpenCL/vendors/pocl.icd
        	fi
	fi

fi

