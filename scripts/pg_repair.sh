#!/bin/bash

ri() {
    local type="inconsistent"
    local list=$(ceph health detail | grep "${type}" | awk '{print $2}')

    for i in ${list}; do
        ceph pg repair "${i}"
    done
}

dr(){
    #ceph pg deep-scrub $(ceph health detail --format json | jq .checks.PG_NOT_DEEP_SCRUBBED.detail[0].message | awk '{print $2}');

    for i in $(ceph health detail | grep since | awk '{print $2}'); do ceph pg deep-scrub ${i}; done;
}

ri;
dr;


